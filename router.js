/**
 * @module router
 */
'use strict';

const userResource = require('./resource/user');
const authResource = require('./auth');

module.exports = (app) => {
    app.use('/users', userResource);
    app.use('/auth', authResource);
};

