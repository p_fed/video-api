/**
 * @module user.controller
 */
'use strict';
const User = require('../../model/user');

exports.findUsers = (req, res) => {
    User.find({})
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.send(err);
        });
};

exports.findUser = (req, res) => {
    User.findById(req.params.id)
        .then((data) => {
            if (!data) {
                return res.status(404).send({error: 'User not found'});
            }
            res.send(data);
        })
        .catch((err) => {
            res.send(err);
        });
};

exports.createUser = (req, res) => {
    User.create(req.body)
        .then((user) => {
            res.send({
                success: true,
                userId: user.id,
                message: 'User ' + user.email + ' has been created'
            });
        })
        .catch((err) => {
            res.send(err);
        });
};

exports.updateUser = (req, res) => {
    const userId = req.params.id;
    const user = req.body;
    User.findByIdAndUpdate(userId, user, {new: true})
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.send(err);
        });
};

exports.deleteUser = (req, res) => {
    const userId = req.params.id;
    User.findByIdAndRemove(userId)
        .then((user) => {
            if (!user) {
                return res.status(404).send({
                    success: false,
                    message: 'User not found'
                });
            }
            res.send({
                success: true,
                message: 'User ' + user.email + ' was deleted'
            });
        })
        .catch((err) => {
            res.send(err);
        });
};