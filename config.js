/**
 * @module config
 */
'use strict';

module.exports = {
    'secret': process.env.SECRET,
    'database': process.env.DATABASE_URL || 'mongodb://localhost:27017/video-api'
};